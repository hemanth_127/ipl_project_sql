-- Number of matches won per team per year in IPL.
with matches_won AS (
    SELECT
        season,
        winner AS team_name,
        COUNT(*) AS matches_won
    FROM
        matches
    WHERE
        winner != ""
    GROUP BY
        season,
        winner
)

-- Select final result
SELECT
    mw.season,
    mw.team_name,
    mw.matches_won
FROM
    matches_won mw
ORDER BY
    mw.season,
    mw.matches_won DESC;

    
