-- Extra runs conceded per team in the year 2016

select 
	deliverydata.bowling_team as bowling_team ,
    sum(extra_runs) as extra_runs 
from 
	matches matchdata
left join
	deliveries deliverydata 
on 
	matchdata.id =deliverydata.match_id
where
	matchdata.season='2017'
group by
	deliverydata.bowling_team;



