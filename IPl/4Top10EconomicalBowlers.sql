-- Top 10 economical bowlers in the year 2015

select 
	bowler ,
    sum(batsman_runs+wide_runs)/(count(*)/6.0) as economy_rate
from 
	deliveries
group by
	bowler
having 
	count(*)>=6
order by
	economy_rate
limit 10;
