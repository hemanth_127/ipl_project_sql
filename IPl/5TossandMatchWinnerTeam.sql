-- Find the number of times each team won the toss and also won the match

select
	Winner as team,
    count(*) as cnt
from
	matches
where 
	toss_winner = winner
group by
	winner
order by
	cnt;



