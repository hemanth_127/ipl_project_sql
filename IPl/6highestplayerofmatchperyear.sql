-- 6.Find a player who has won the highest number of Player of the Match awards for each season 

with cte as (
select season,player_of_match,count(player_of_match) as cnt from matches
group by season,player_of_match
)
,max_cnt as (
	select season,max(cnt) as c from cte
    group by season
)
select cte.season,cte.player_of_match,cnt from cte
join max_cnt on cte.season=max_cnt.season and max_cnt.c =cnt
group by cte.season,cte.player_of_match

