-- Find the strike rate of a batsman for each season
with strike as(
select season ,batsman,(sum(batsman_runs)/count(ball))*100 as strike_rate
from matches
join deliveries on matches.id = deliveries.match_id 
group by season,batsman
order by strike_rate Desc
),
max_strike as(
select season,max(strike_rate) as maximum_strike_rate from strike 
group by season
 )
 
 select  * from strike 
 join max_strike on max_strike.season = strike.season and maximum_strike_rate=strike_rate
