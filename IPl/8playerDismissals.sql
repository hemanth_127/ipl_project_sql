-- Find the highest number of times one player has been dismisseddismissal_kind by another player

select  batsman , bowler,count(bowler) as dismissed_count from deliveries 
group by batsman ,bowler
order by dismissed_count desc;
