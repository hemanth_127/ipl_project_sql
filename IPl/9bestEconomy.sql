-- Find the bowler with the best economy in super overs

with super_overs as(
select *
from deliveries
where is_super_over != 0
)
select bowler,sum(wide_runs + noball_runs + batsman_runs + legbye_runs + bye_runs)/(count(ball)/6.0) as economy 
from super_overs
group by bowler
having sum(wide_runs + noball_runs + batsman_runs + legbye_runs + bye_runs)/(count(ball)/6.0) >0
order by economy
limit 1;
